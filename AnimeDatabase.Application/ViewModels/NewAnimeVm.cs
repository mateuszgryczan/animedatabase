﻿using AnimeDatabase.Application.Mapping;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnimeDatabase.Application.ViewModels
{
    public class NewAnimeVm : IMapFrom<AnimeDatabase.Domain.Model.Anime>
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int TypeId { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<NewAnimeVm, AnimeDatabase.Domain.Model.Anime>()
                .ForMember(a => a.TypeId, opt => opt.MapFrom(b => 1));
        }
    }
}
