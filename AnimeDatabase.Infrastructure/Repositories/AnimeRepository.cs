﻿using AnimeDatabase.Domain.Interface;
using AnimeDatabase.Domain.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Type = AnimeDatabase.Domain.Model.Type;

namespace AnimeDatabase.Infrastructure.Repositories
{
    public class AnimeRepository : IAnimeRepository
    {
        private readonly Context _context;

        public AnimeRepository(Context context)
        {
            _context = context;
        }

        public int AddAnime(Anime anime)
        {
            _context.Animes.Add(anime);
            _context.SaveChanges();

            return anime.Id;
        }

        public void DeleteAnime(int animeId)
        {
            var anime = _context.Animes.Find(animeId);

            if (anime != null)
            {
                _context.Animes.Remove(anime);
                _context.SaveChanges();
            }
        }

        public Anime GetAnime(int animeId)
        {
            var anime = _context.Animes.Include(a => a.Type)
                .Include(a => a.AnimeDetails)
                .FirstOrDefault(a => a.Id == animeId);

            return anime;
        }

        public IQueryable<Anime> GetAnimesByTypeId(int typeId)
        {
            var animes = _context.Animes.Where(a => a.TypeId == typeId);

            return animes;
        }


        public IQueryable<Tag> GetAllTags()
        {
            var tags = _context.Tags;

            return tags;
        }

        public IQueryable<Type> GetAllTypes()
        {
            var types = _context.Types;

            return types;
        }

        public IQueryable<Anime> GetAllAnimes()
        {
            return _context.Animes;
        }
    }
}
