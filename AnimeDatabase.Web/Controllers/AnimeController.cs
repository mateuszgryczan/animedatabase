﻿using AnimeDatabase.Application.Interfaces;
using AnimeDatabase.Application.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnimeDatabase.Web.Controllers
{
    public class AnimeController : Controller
    {
        private readonly IAnimeService _animeService;

        public AnimeController(IAnimeService animeService)
        {
            _animeService = animeService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var model = _animeService.GetAllAnimesForList(2, 1, "");
            
            return View(model);
        }

        [HttpPost]
        public IActionResult Index(int pageSize, int? pageNumber, string searchString)
        {
            if (!pageNumber.HasValue)
            {
                pageNumber = 1;
            }

            if (searchString is null)
            {
                searchString = String.Empty;
            }

            var model = _animeService.GetAllAnimesForList(pageSize, pageNumber.Value, searchString);

            return View(model);
        }

        [HttpGet]
        public IActionResult AddAnime()
        {
            return View(new NewAnimeVm());
        }

        [HttpPost]
        public IActionResult AddAnime(NewAnimeVm model)
        {
            var id = _animeService.AddAnime(model);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult View(int id)
        {
            var model = _animeService.GetAnimeDetails(id);

            return View(model);
        }
    }
}
